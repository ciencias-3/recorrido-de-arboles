//Recorrido en inorden, preorden y postorden de un arbol binario

//Josue Nuñez prada 20172020071
//Diego Alonso Galeano Herrera 20172020074
//Santiago Rincon Robelto 20172020084

//Los numeros se deben ingresar en un solo string
//EJ: 1,2,3,4,5,6,7

//Se utilizó trabajo del semestre anterior para poder realizar la interfaz grafica

package launcher;

import vista.VistaPrincipal;

public class Main {

    public static void main(String[] args) {
        detalles();
        VistaPrincipal ventana = new VistaPrincipal();
        ventana.setVisible(true);
    }

    public static void detalles() {
        // TODO ESTO HACE VER LA VENTANA BONITA QUITAR PARA PROBAR COMO SE VE COMO SE VE
        // NORMAL
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VistaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null,
                    ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VistaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null,
                    ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VistaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null,
                    ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VistaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null,
                    ex);
        }
    }
}
