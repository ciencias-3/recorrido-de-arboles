package logica;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.LinkedList;

public class Arbol {

	private Nodo raiz;
	private LinkedList<String> grafica = new LinkedList<String>();

	public void insertar(int llave) {
		insertar(llave, raiz);
	}

	private Nodo insertar(int n, Nodo A) {
		if (A == null) {
			A = new Nodo();
			if (raiz == null) {
				raiz = A;
			}
			A.Cambiar_llave(n);
			return A;
		} else {
			if (n <= A.llave) {
				if (A.izquierda == null) {
					A.izquierda = new Nodo();
					A.izquierda.Cambiar_llave(n);
				} else {
					insertar(n, A.izquierda);
				}
			} else {
				if (A.derecha == null) {
					A.derecha = new Nodo();
					A.derecha.Cambiar_llave(n);
				} else {
					insertar(n, A.derecha);
				}
			}
		}
		return null;
	}

	public Nodo RetornarRaiz() {
		return raiz;
	}

	public LinkedList<String> getDatosGrafica() {
		grafica.clear();
		grafica.add(Integer.toString(raiz.llave));
		return getDatosGrafica(raiz);
	}

	private LinkedList<String> getDatosGrafica(Nodo A) {

		if (A != null) {
			if (A.izquierda != null) {
				grafica.add(Integer.toString(A.izquierda.llave));
			}
			if (A.derecha != null) {
				grafica.add(Integer.toString(A.derecha.llave));
			}
			getDatosGrafica(A.izquierda);
			getDatosGrafica(A.derecha);
		}
		return grafica;
	}

}
