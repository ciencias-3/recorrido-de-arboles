package logica;

public class Nodo {
    ////////////////////VARIABLES MANEJO//////////////////////////////// 
    int llave;
    Nodo izquierda;
    Nodo derecha;
    //Nodo AsociacionABB;//este es el apuntador para asociar el Nodo del Arbl AVL "noticia" con el Arbol de "complementos" ABB
    ////////////////////VARIABLES DE INFORMACION/////////////////////////
    String Titulo,Descripcion,Path;
    Nodo(){
        llave=0;
        izquierda=null;
        derecha=null;
    }

    public void Cambiar_llave(int llave){
        this.llave=llave;
    }

    public void Cambiar_izquierda(Nodo izquierda){
        this.izquierda=izquierda;
    }

    public void Cambiar_derecha(Nodo derecha){
        this.derecha=derecha;
    }
}
